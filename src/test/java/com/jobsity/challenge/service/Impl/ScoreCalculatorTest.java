//package com.jobsity.challenge.service.Impl;
//
//import com.jobsity.challenge.exceptions.InvalidFramesException;
//import com.jobsity.challenge.service.ILastFrameHandlerService;
//import com.jobsity.challenge.service.IValidator;
//import com.jobsity.challenge.service.impl.LastFrameHandlerService;
//import com.jobsity.challenge.service.impl.ScoreCalculatorService;
//import com.jobsity.challenge.service.impl.Validator;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//
//import java.util.AbstractMap;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.doAnswer;
//
//public class ScoreCalculatorTest {
//
//    @Mock
//    private LastFrameHandlerService lastFrameHandlerService;
//
//    @Mock
//    private Validator validator;
//
//    @InjectMocks
//    private ScoreCalculatorService scoreCalculator;
//
//    private static final String PLAYER = "Player";
//
//    @Before
//    public void initMocks() {
//        MockitoAnnotations.initMocks(this);
//    }
//
//    @Test(expected = InvalidFramesException.class)
//    public void calculateScore_invalidFramesInput_throwInvalidFramesException() throws Exception {
//        final String[] shots = {"10", "10", "10", "10", "10", "10", "10"};
//        scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER);
//    }
//
//    @Test
//    public void calculateScore_perfectScore() throws Exception {
//        final String[] shots = {"10", "10", "10", "10", "10", "10", "10", "10", "10", "10", "10", "10"};
//        Assert.assertEquals(Arrays.asList(30, 60, 90, 120, 150, 180, 210, 240, 270, 300),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("PINFALL_STRIKE", "PINFALL_STRIKE", "PINFALL_STRIKE")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_firstFrameSpare() throws Exception {
//        final String[] shots = {"5", "5", "10", "10", "10", "10", "10", "10", "10", "10", "10", "10", "10"};
//        Assert.assertEquals(Arrays.asList(20, 50, 80, 110, 140, 170, 200, 230, 260, 290),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("5", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("PINFALL_STRIKE", "PINFALL_STRIKE", "PINFALL_STRIKE")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_allSpares() throws Exception {
//        final String[] shots = {"5", "5", "5", "5", "5", "5", "5", "5", "5", "5", "5", "5", "5", "5", "5",
//                "5", "5", "5", "5", "5", "5"};
//        Assert.assertEquals(Arrays.asList(15, 30, 45, 60, 75, 90, 105, 120, 135, 150),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("5", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("5", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("5", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("5", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("5", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("5", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("5", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("5", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("5", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("5", "/", "5")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_allFramesWithOnePinFall() throws Exception {
//        final String[] shots = {"1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1",
//                "1", "1", "1", "1", "1", "1"};
//        Assert.assertEquals(Arrays.asList(2, 4, 6, 8, 10, 12, 14, 16, 18, 20),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("1", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("1", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("1", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("1", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("1", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("1", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("1", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("1", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("1", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("1", "1")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_lastFrameTwoAttemptsWithOnePinFall() throws Exception {
//        final String[] shots = {"10", "10", "10", "10", "10", "10", "10", "10", "10", "1", "1"};
//        Assert.assertEquals(Arrays.asList(30, 60, 90, 120, 150, 180, 210, 231, 243, 245),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("1", "1")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_lastFrameWithSpareAndLastShotZeroPinFalls() throws Exception {
//        final String[] shots = {"10", "10", "10", "10", "10", "10", "10", "10", "10", "5", "5", "0"};
//        Assert.assertEquals(Arrays.asList(30, 60, 90, 120, 150, 180, 210, 235, 255, 265),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("5", "/", "0")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_firstFramesZeroPinFallsEightFrameStrikeNineFrameStrikeLastFrameTwoPinFalls() throws Exception {
//        final String[] shots = {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0",
//                "10", "10", "1", "1"};
//        Assert.assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 21, 33, 35),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("1", "1")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_worstScore() throws Exception {
//        final String[] shots = {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0",
//                "0", "0", "0", "0", "0", "0"};
//        Assert.assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("0", "0")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_allFouls() throws Exception {
//        final String[] shots = {"F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F",
//                "F", "F", "F", "F", "F", "F"};
//        Assert.assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("F", "F")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("F", "F")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("F", "F")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("F", "F")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("F", "F")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("F", "F")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("F", "F")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("F", "F")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("F", "F")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("F", "F")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_scoreWithSparesAndWithOnePinFallFrames() throws Exception {
//        final String[] shots = {"0", "1", "4", "6", "0", "1", "4", "6", "0", "1", "4", "6", "0", "1",
//                "4", "6", "0", "1", "4", "6", "0"};
//        Assert.assertEquals(Arrays.asList(1, 11, 12, 22, 23, 33, 34, 44, 45, 55),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("0", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("4", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("0", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("4", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("0", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("4", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("0", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("4", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("0", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("4", "/", "0")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_scoreWithNonePinFallsAndStrikes() throws Exception {
//        final String[] shots = {"0", "0", "10", "0", "0", "10", "0", "0", "10", "0", "0", "10", "0",
//                "0", "10", "0", "10"};
//        Assert.assertEquals(Arrays.asList(0, 10, 10, 20, 20, 30, 30, 40, 40, 60),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("PINFALL_STRIKE", "0", "/")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_scoreOnlyInLastFrame() throws Exception {
//        final String[] shots = {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0",
//                "0", "0", "0", "0", "10", "5", "5"};
//        Assert.assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 20),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("0", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("PINFALL_STRIKE", "5", "/")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_allSparesWithZeroPinFallsInEachFirstAttempt() throws Exception {
//        final String[] shots = {"0", "10", "0", "10", "0", "10", "0", "10", "0", "10", "0", "10", "0",
//                "10", "0", "10", "0", "10", "0", "10", "0"};
//        Assert.assertEquals(Arrays.asList(10, 20, 30, 40, 50, 60, 70, 80, 90, 100),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("0", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("0", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("0", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("0", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("0", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("0", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("0", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("0", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("0", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("0", "/", "0")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_jeffSampleScore() throws Exception {
//        final String[] shots = {"10", "7", "3", "9", "0", "10", "0", "8", "8", "2", "F", "6", "10",
//                "10", "10", "8", "1"};
//        Assert.assertEquals(Arrays.asList(20, 39, 48, 66, 74, 84, 90, 120, 148, 167),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("7", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("9", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("0", "8")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("8", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("F", "6")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("PINFALL_STRIKE", "8", "1")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//    @Test
//    public void calculateScore_johnSampleScore() throws Exception {
//        final String[] shots = {"3", "7", "6", "3", "10", "8", "1", "10", "10", "9", "0", "7", "3",
//                "4", "4", "10", "9", "0"};
//        Assert.assertEquals(Arrays.asList(16, 25, 44, 53, 82, 101, 110, 124, 132, 151),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getScore());
//        Assert.assertEquals(Stream.of(
//                new AbstractMap.SimpleImmutableEntry<>(1, Arrays.asList("3", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(2, Arrays.asList("6", "3")),
//                new AbstractMap.SimpleImmutableEntry<>(3, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(4, Arrays.asList("8", "1")),
//                new AbstractMap.SimpleImmutableEntry<>(5, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(6, Arrays.asList("PINFALL_STRIKE")),
//                new AbstractMap.SimpleImmutableEntry<>(7, Arrays.asList("9", "0")),
//                new AbstractMap.SimpleImmutableEntry<>(8, Arrays.asList("7", "/")),
//                new AbstractMap.SimpleImmutableEntry<>(9, Arrays.asList("4", "4")),
//                new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("PINFALL_STRIKE", "9", "0")))
//                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
//                scoreCalculator.calculateScore(Arrays.asList(shots), PLAYER).getFramesShots());
//    }
//
//
//}
