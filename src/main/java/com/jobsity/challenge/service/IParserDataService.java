package com.jobsity.challenge.service;

import com.jobsity.challenge.exceptions.InvalidScoreException;
import com.jobsity.challenge.model.PlayerShots;

import java.util.List;

public interface IParserDataService {

    List<PlayerShots> getPlayersShots(final List<String> lines) throws InvalidScoreException;

}
