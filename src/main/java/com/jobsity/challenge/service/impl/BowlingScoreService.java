package com.jobsity.challenge.service.impl;

import com.jobsity.challenge.model.FramesResult;
import com.jobsity.challenge.model.PlayerShots;
import com.jobsity.challenge.service.IBowlingScoreService;
import com.jobsity.challenge.service.IPrinterService;
import com.jobsity.challenge.service.IScoreCalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class BowlingScoreService implements IBowlingScoreService {

    private IScoreCalculatorService scoreCalculatorService;
    private IPrinterService printerService;

    @Autowired
    public void setScoreCalculator(IScoreCalculatorService scoreCalculatorService) {
        this.scoreCalculatorService = scoreCalculatorService;
    }

    @Autowired
    public void setPrinterService(IPrinterService printerService) {
        this.printerService = printerService;
    }

    public void calculateScore(final List<PlayerShots> playersShots) {

        final List<FramesResult> framesResults =
                playersShots.stream().map(playerShots -> scoreCalculatorService.calculateScore(playerShots))
                        .collect(Collectors.toList());

        printerService.printResults(framesResults);

    }
}
