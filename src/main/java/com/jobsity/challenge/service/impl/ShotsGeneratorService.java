package com.jobsity.challenge.service.impl;

import com.jobsity.challenge.model.Shot;
import com.jobsity.challenge.service.IShotsGeneratorService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class ShotsGeneratorService implements IShotsGeneratorService {

    public List<Shot> getShotSequenceForProcessing(List<String> shots, int shotIndex) {
        Shot actualShot = Shot.createShot(shots.get(shotIndex));
        Shot nextShot = Shot.createShot(shots.get(shotIndex + 1));
        Shot afterNextShot = shotIndex + 2 < shots.size() ?
                Shot.createShot(shots.get(shotIndex + 2)) : Shot.createEmptyShot();

        return Arrays.asList(actualShot, nextShot, afterNextShot);
    }
}
