package com.jobsity.challenge.model;

import java.util.List;

public class FramesResult {

    private List<Integer> score;
    private FrameShots frameShots;
    private String player;

    protected FramesResult(List<Integer> score, FrameShots frameShots, String player) {
        this.score = score;
        this.frameShots = frameShots;
        this.player = player;
    }

    public static FramesResult createFrameResult(List<Integer> score, FrameShots framesShots, String player) {
        return new FramesResult(score, framesShots, player);
    }

    public List<Integer> getScore() {
        return score;
    }

    public FrameShots getFrameShots() {
        return frameShots;
    }

    public String getPlayer() {
        return player;
    }

    @Override
    public String toString() {
        return "FramesResult{" +
                "score=" + score +
                ", frameShots=" + frameShots +
                ", player='" + player + '\'' +
                '}';
    }
}
