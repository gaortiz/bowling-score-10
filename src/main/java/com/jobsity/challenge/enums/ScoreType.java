package com.jobsity.challenge.enums;

public enum ScoreType {
    STRIKE("X", "10", 10),
    FOUL("F", "F", 0),
    SPARE("/", "", 10),
    EMPTY_SHOT("", "", 0);

    public final String label;
    public final String inputValue;
    public final Integer value;

    ScoreType(String label, String inputValue, Integer value) {
        this.label = label;
        this.inputValue = inputValue;
        this.value = value;
    }
}
